//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http:           //www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: May 29, 2021
//
//======================================================================================================================
#include <thread>
using namespace std::literals;

#include <Trace.h>
#include <rvr++.h>
//---------------------------------------------------------------------------------------------------------------------
void direct(rvr::SensorsDirect& sen_d);
void driving(rvr::Drive& drive, rvr::SensorsDirect& sen_d);
void notifications(rvr::SensorsDirect& sen_d);
void power(rvr::Power& pow);
void streaming(rvr::SensorsStream& sen_s, rvr::SensorsDirect& sen_d);
void specials(rvr::Blackboard& bb, rvr::ApiShell& api, rvr::SensorsDirect& sen_d, rvr::SensorsStream& sen_s);
void sysinfo(rvr::SystemInfo& sys, rvr::Connection& con, rvr::ApiShell& api);
//---------------------------------------------------------------------------------------------------------------------
int main(int argc, char* argv[]) {
    mys::tout << code_entry;

    if (argc < 2) {
        std::cout << "=============================\n";
        std::cout << "Usage:\n";
        std::cout << "\trvrDemo /dev/<serial port>\n";
        std::cout << "\trvrDemo <IP> <port>\n";
        std::cout << "=============================\n";
        return 0;
    }


    mys::TraceOn tdbg_ctrl { mys::tdbg };
    //---------------------------------------------------------------------------------------------------------------------
    auto stream = rvr::Stream::make_stream(argc, argv);
    rvr::ReadPacket packet_stream { *stream };
    rvr::SendPacket requests { *stream };
    //---------------------------------------------------------------------------------------------------------------------
    rvr::Blackboard bb;
    rvr::ApiShell api(bb, requests);
    rvr::Connection conn(bb, requests);
    rvr::Drive drive(bb, requests);
    rvr::Power pow(bb, requests);
    rvr::SensorsDirect sen_d(bb, requests);
    rvr::SensorsStream sen_s(bb, requests);
    rvr::SystemInfo sys(bb, requests);
    //---------------------------------------------------------------------------------------------------------------------
    // wake up the RVR if aslweep
    pow.awaken();
    sen_d.disableEverything(rvr::CommandResponse::resp_yes);
    //---------------------------------------------------------------------------------------------------------------------
    //  Construct Response which will start response thread
    rvr::Response response { packet_stream, bb };

    pow.wait_for( &rvr::Power::isAwake);
    mys::tout << code_line << "is awake:  " << pow.isAwake();
    //=====================================================================================================================
    try {

        mys::tout << code_line << "done";
    }
    catch (std::exception& e) {
        mys::terr << code_line << e.what() << "=================================";
    }

    mys::tout << code_line << "sleep";

    pow.resetSleepNotify();
    pow.sleep(rvr::CommandResponse::resp_yes);

    bb.dump();

    mys::tout << code_line << "wait for sleep notify";
    pow.wait_for( &rvr::Power::isDidSleepNotify);

    response.shutdown();

    mys::tout << code_exit;

    return 0;
}
