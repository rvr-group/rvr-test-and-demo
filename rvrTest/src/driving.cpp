//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//		 File: drive.cpp
//
//     Author: rmerriam
//
//    Created: Oct 29, 2021
//
//======================================================================================================================
#include <thread>

#include <rvr++.h>

using enum rvr::CommandResponse;
//---------------------------------------------------------------------------------------------------------------------
auto const drive_pos { 0.5 };
auto const drive_speed { 2.0 };
//---------------------------------------------------------------------------------------------------------------------
void cmds(rvr::Drive& drive) {
    mys::tdbg << code_entry;

    mys::tout << code_line << "yaw drive";
    drive.driveYawNormalized(30, 30, resp_yes);
    drive.driveYawSI(30.0, 0.25, resp_yes);

//    mys::tout << code_line << "drive to position";
//    driveToPositionNormalized
//    driveToPositionSI

//    drive
//    driveWithHeading
    std::this_thread::sleep_for(2000ms);

    mys::tdbg << code_exit;
}
//---------------------------------------------------------------------------------------------------------------------
void check_control_system(rvr::Drive& drive) {
    mys::tdbg << code_entry;

    drive.getActiveControlSystemId();
    std::this_thread::sleep_for(50ms);
    mys::tout << code_line << (uint8_t)drive.activeControlSystemId().get_or() << mys::tab << drive.activeControlSystemIdText().get_or();
    mys::tdbg << code_exit;
}
//---------------------------------------------------------------------------------------------------------------------
void rc(rvr::Drive& drive) {
    mys::tdbg << code_entry;

    for (auto cnt { 0 }; cnt < 3; ++cnt) {
        drive.driveRcSI(30, 0.25, 0, resp_yes);
        std::this_thread::sleep_for(1500ms);
    }

    for (auto cnt { 0 }; cnt < 3; ++cnt) {
        drive.driveRcSI( -30, -0.25, 0, resp_yes);
        std::this_thread::sleep_for(1500ms);
    }

    drive.stop();
    drive.wait_for( &rvr::Drive::isStopControllerStopped);

    mys::tdbg << code_exit;
}
//---------------------------------------------------------------------------------------------------------------------
void yaw(rvr::Drive& drive) {
    mys::tdbg << code_entry;

    for (auto cnt { 0 }; cnt < 3; ++cnt) {
        drive.driveYawSI(30, 0.25, resp_yes);
        std::this_thread::sleep_for(500ms);
    }

    for (auto cnt { 0 }; cnt < 3; ++cnt) {
        drive.driveYawSI(30, -0.25, resp_yes);
        std::this_thread::sleep_for(500ms);
    }

    drive.stop();
    drive.wait_for( &rvr::Drive::isStopControllerStopped);

    mys::tdbg << code_exit;
}
//---------------------------------------------------------------------------------------------------------------------
void control(rvr::Drive& drive) {

    mys::tdbg << code_entry;

    drive.getStopControllerState(resp_yes);
    std::this_thread::sleep_for(50ms);
    mys::tout << code_line << "isStopControllerStopped" << mys::tab << drive.isStopControllerStopped().get_or();

    drive.restoreDefaultControlSystemTimeout(resp_yes);

    drive.driveYawNormalized(0, 0, resp_yes);
//    drive.driveYawSI(30.0, 0.25, resp_yes);

    drive.getStopControllerState(resp_yes);
    std::this_thread::sleep_for(5000ms);
    mys::tout << code_line << "isStopControllerStopped" << mys::tab << drive.isStopControllerStopped().get_or();

    mys::tout << code_line << "getActiveControlSystemId";

    drive.restoreInitialDefaultControlSystems(resp_yes);
    check_control_system(drive);

    drive.setActiveControlSystemId(rvr::Drive::control_system_type_drive_with_yaw, rvr::Drive::ControlSystemId::drive_with_yaw_advanced_mode,
                                   resp_yes);
    drive.driveYawNormalized(0, 0, resp_yes);

    check_control_system(drive);
    drive.stop();

    drive.setActiveControlSystemId(rvr::Drive::control_system_type_drive_with_yaw, rvr::Drive::ControlSystemId::drive_with_yaw_basic_mode, resp_yes);
    drive.driveYawNormalized(0, 0, resp_yes);

    check_control_system(drive);

    drive.setCustomControlSystemTimeout(500, resp_yes);
    drive.driveYawNormalized(0, 0, resp_yes);
    std::this_thread::sleep_for(500ms);

    mys::tdbg << code_exit;
}
//---------------------------------------------------------------------------------------------------------------------
void slew(rvr::Drive& drive) {
    mys::tdbg << code_entry;

    // get default params
    drive.restoreDefaultDriveTargetSlewParameters(resp_yes);
    std::this_thread::sleep_for(100ms);

    drive.getDriveTargetSlewParameters(resp_yes);
//    std::this_thread::sleep_for(150ms);

    {
        while (drive.targetSlewParameters().invalid()) {
            std::this_thread::sleep_for(25ms);
        }
        auto [a, b, c, l, f] = drive.targetSlewParameters().get_or();
        mys::tout << code_line << "targetSlewParameters" << mys::sp << a << mys::sp << b << mys::sp << c << mys::sp << l << mys::sp << f;
    }

    // set new params
    rvr::Drive::SlewRateParams slew_params = {    //
    .a = -160,    //
        .b = 200,    //
        .c = 100,    //
        .linear_acceleration = 4,    //
        .method = rvr::Drive::LinearVelocitySlewMethods::proportional    //
    };

    drive.setDriveTargetSlewParameters(slew_params, resp_yes);
    std::this_thread::sleep_for(150ms);

    drive.getDriveTargetSlewParameters(resp_yes);
    {
        while (drive.targetSlewParameters().invalid()) {
            std::this_thread::sleep_for(25ms);
        }
        auto [a, b, c, l, f] = drive.targetSlewParameters().get_or();
        mys::tout << code_line << "targetSlewParameters" << mys::sp << a << mys::sp << b << mys::sp << c << mys::sp << l << mys::sp << f;
    }
    mys::tdbg << code_exit;
}
//---------------------------------------------------------------------------------------------------------------------
void magnetometer(rvr::Drive& drive, rvr::SensorsDirect& sen_d) {
    mys::tdbg << code_entry;

    drive.driveToPositionSI(125, 0.0, 0.0, drive_speed, rvr::Drive::ModeFlags::forward, resp_yes);
    drive.wait_for( &rvr::Drive::isDriveToPositionDone, 3000);

    sen_d.calibrateMagnetometer(resp_yes);
    sen_d.wait_for( &rvr::SensorsDirect::isMagnetometerCalibrationDone, 3000);

    auto yaw { sen_d.magnetometerCalibrationYaw().get_or() };
    mys::tout << code_line << "mag rc " << yaw << mys::sp << 180 - yaw << mys::sp << yaw - 180;
    mys::tout.flush();

    std::this_thread::sleep_for(500ms);

    drive.driveToPositionSI(yaw, 0.0, 0.0, drive_speed, rvr::Drive::ModeFlags::forward, resp_yes);
    drive.wait_for( &rvr::Drive::isDriveToPositionDone, 3000);

    mys::tout << code_line << "yaw to north done";
    mys::tout.flush();

    drive.resetYaw(resp_yes);
    drive.driveToPositionSI(180, 0.0, 0.0, drive_speed, rvr::Drive::ModeFlags::forward, resp_yes);
    drive.wait_for( &rvr::Drive::isDriveToPositionDone, 3000);

    mys::tdbg << code_exit;
}
//---------------------------------------------------------------------------------------------------------------------
void location(rvr::Drive& drive, rvr::SensorsDirect& sen_d) {
    mys::tdbg << code_entry;

    // out and back on y asix in forward and reverse
    drive.driveToPositionSI(0.0, 0.0, drive_pos, drive_speed, rvr::Drive::ModeFlags::forward, resp_yes);
    while (drive.isDriveToPositionDone().invalid()) {
        std::this_thread::sleep_for(25ms);
    }

    drive.driveToPositionSI(180, 0.0, 0.0, drive_speed, rvr::Drive::ModeFlags::forward, resp_yes);
    while (drive.isDriveToPositionDone().invalid()) {
        std::this_thread::sleep_for(25ms);
    }

    // out and back to left on X asix in forward
    drive.driveToPositionSI(90, -drive_pos, 0.0, drive_speed, rvr::Drive::ModeFlags::forward, resp_yes);
    while (drive.isDriveToPositionDone().invalid()) {
        std::this_thread::sleep_for(25ms);
    }

    drive.driveToPositionSI( -45, 0.0, 0.0, drive_speed, rvr::Drive::ModeFlags::forward, resp_yes);
    while (drive.isDriveToPositionDone().invalid()) {
        std::this_thread::sleep_for(25ms);
    }

    // out and back to righ 45 degrees in forward
    drive.driveToPositionSI( -45, drive_pos, drive_pos, drive_speed, rvr::Drive::ModeFlags::forward, resp_yes);
    while (drive.isDriveToPositionDone().invalid()) {
        std::this_thread::sleep_for(25ms);
    }

    drive.driveToPositionSI(0, 0.0, 0.0, drive_speed, rvr::Drive::ModeFlags::reverse, resp_yes);
    while (drive.isDriveToPositionDone().invalid()) {
        std::this_thread::sleep_for(25ms);
    }

    std::this_thread::sleep_for(250ms);

    mys::tdbg << code_exit;
}
//----------------------------------------
void driving(rvr::Drive& drive, rvr::SensorsDirect& sen_d) {
    mys::tdbg << code_entry;

    sen_d.resetLocatorXY(resp_yes);
    drive.resetYaw(resp_yes);
    drive.enableMotorStallNotify(resp_yes);
    drive.restoreDefaultControlSystemTimeout(resp_yes);
    drive.restoreDefaultDriveTargetSlewParameters(resp_yes);
    std::this_thread::sleep_for(100ms);

    slew(drive);

    mys::TraceOn tdbg_ctrl { mys::tdbg };
    magnetometer(drive, sen_d);

//    cmds(drive);
//    control(drive);
//    location(drive, sen_d);
//    rc(drive);
//    yaw(drive);

    mys::tdbg << code_exit;
}
