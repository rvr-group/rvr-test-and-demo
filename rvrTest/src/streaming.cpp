//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//		 File: streaming.cpp
//
//     Author: rmerriam
//
//    Created: Jun 16, 2021
//
//======================================================================================================================
#include <thread>

#include <rvr++.h>
//---------------------------------------------------------------------------------------------------------------------
void nordic_ambient_light(rvr::SensorsStream& sen_s) {
    mys::tdbg << code_entry;

    sen_s.disableStreamingNordic();
    sen_s.clearStreamingNordic(rvr::CommandResponse::resp_yes);

    sen_s.streamAmbient(rvr::CommandResponse::resp_yes);
    sen_s.startStreamingNordic(50, rvr::CommandResponse::resp_yes);
    sen_s.wait_for( &rvr::SensorsStream::ambient);

    sen_s.disableStreamingNordic();
    sen_s.clearStreamingNordic();

    mys::tout << code_line << "ambient: "s << sen_s.ambient().get_or();

    mys::tdbg << code_exit;
}
//---------------------------------------------------------------------------------------------------------------------
void nordic_color(rvr::SensorsStream& sen_s, rvr::SensorsDirect& sen_d) {
    mys::tdbg << code_entry;

    sen_s.disableStreamingNordic();
    sen_s.clearStreamingNordic(rvr::CommandResponse::resp_yes);

    sen_s.enableColorDetection(rvr::CommandResponse::resp_yes);
    sen_s.streamColor(rvr::CommandResponse::resp_yes);

    sen_s.startStreamingNordic(50, rvr::CommandResponse::resp_yes);
    sen_s.wait_for( &rvr::SensorsStream::colors);

    sen_s.disableStreamingNordic();
    sen_s.clearStreamingNordic(rvr::CommandResponse::resp_yes);
    sen_s.disableColorDetection(rvr::CommandResponse::resp_yes);

    auto [d_r, d_g, d_b, index, conf] { sen_s.colors().get_or() };
    mys::tout << code_line << "streaming colors: " << std::hex << std::showbase << (int)(d_r) << mys::sp << (int)(d_g) << mys::sp << (int)(d_b)
        << mys::sp << (int)(index) << std::noshowbase << mys::sp << conf;

    mys::tdbg << code_exit;
}
//---------------------------------------------------------------------------------------------------------------------
void nordic_time(rvr::SensorsStream& sen_s) {
    mys::tdbg << code_entry;

    sen_s.disableStreamingNordic(rvr::CommandResponse::resp_yes);
    sen_s.clearStreamingNordic(rvr::CommandResponse::resp_yes);

    sen_s.streamNordicTime(rvr::CommandResponse::resp_yes);

    sen_s.startStreamingNordic(50, rvr::CommandResponse::resp_yes);
    sen_s.wait_for( &rvr::SensorsStream::nordicTime);

    sen_s.disableStreamingNordic(rvr::CommandResponse::resp_yes);
    sen_s.clearStreamingNordic(rvr::CommandResponse::resp_yes);

    auto ct { sen_s.nordicTime().get_or() };

    mys::tout << code_line << "Nordic Core Time: "s << std::dec << ct;

    mys::tdbg << code_exit;
}
//---------------------------------------------------------------------------------------------------------------------
void bt_time(rvr::SensorsStream& sen_s) {
    mys::tdbg << code_entry;

    sen_s.disableStreamingBT(rvr::CommandResponse::resp_yes);
    sen_s.clearStreamingBT(rvr::CommandResponse::resp_yes);

    sen_s.streamBtTime(rvr::CommandResponse::resp_yes);

    sen_s.startStreamingBT(50, rvr::CommandResponse::resp_yes);
    sen_s.wait_for( &rvr::SensorsStream::btTime);

    sen_s.disableStreamingBT(rvr::CommandResponse::resp_yes);
    sen_s.clearStreamingBT(rvr::CommandResponse::resp_yes);

    auto ct { sen_s.btTime().get_or() };
    mys::tout << code_line << "BT Core Time: "s << ct;

    mys::tdbg << code_exit;
}
//---------------------------------------------------------------------------------------------------------------------
void bt_imu_accel_gyro(rvr::SensorsStream& sen_s) {
    mys::tdbg << code_entry;

    sen_s.disableStreamingBT(rvr::CommandResponse::resp_yes);
    sen_s.clearStreamingBT(rvr::CommandResponse::resp_yes);

    sen_s.streamImuAccelGyro(rvr::CommandResponse::resp_yes);

    sen_s.startStreamingBT(50, rvr::CommandResponse::resp_yes);
    sen_s.wait_for( &rvr::SensorsStream::accelerometer);

    sen_s.disableStreamingBT(rvr::CommandResponse::resp_yes);
    sen_s.clearStreamingBT(rvr::CommandResponse::resp_yes);

    auto [a_x, a_y, a_z] { sen_s.accelerometer().get_or() };
    mys::tout << code_line << "accelerometer: " << a_x << mys::sp << a_y << mys::sp << a_z;

    auto [g_x, g_y, g_z] { sen_s.gyroscope().get_or() };
    mys::tout << code_line << "gyroscope: " << g_x << mys::sp << g_y << mys::sp << g_z;

    auto [i_x, i_y, i_z] { sen_s.imu().get_or() };
    mys::tout << code_line << "imu: " << i_x << mys::sp << i_y << mys::sp << i_z;

    mys::tdbg << code_exit;
}
//---------------------------------------------------------------------------------------------------------------------
void bt_speed_vel_loc(rvr::SensorsStream& sen_s) {
    mys::tdbg << code_entry;

    sen_s.disableStreamingBT(rvr::CommandResponse::resp_yes);
    sen_s.clearStreamingBT(rvr::CommandResponse::resp_yes);

    sen_s.streamVelocityLocatorSpeed(rvr::CommandResponse::resp_on_error);
    sen_s.streamEncoders(rvr::CommandResponse::resp_on_error);

    sen_s.startStreamingBT(50, rvr::CommandResponse::resp_on_error);
    sen_s.wait_for( &rvr::SensorsStream::velocity);
    sen_s.wait_for( &rvr::SensorsStream::encoders);

    sen_s.disableStreamingBT(rvr::CommandResponse::resp_yes);
    sen_s.clearStreamingBT(rvr::CommandResponse::resp_yes);

    auto [e_left, e_right] { sen_s.encoders().get_or() };
    mys::tout << code_line << "encoders: " << e_left << mys::sp << e_right;

    auto [l_x, l_y] { sen_s.locator().get_or() };
    mys::tout << code_line << "locator: " << l_x << mys::sp << l_y;

    auto [v_x, v_y] { sen_s.velocity().get_or() };
    mys::tout << code_line << "velocity: " << v_x << mys::sp << v_y;

    mys::tdbg << code_exit;
}
//---------------------------------------------------------------------------------------------------------------------
void bt_quaternion(rvr::SensorsStream& sen_s) {
    mys::tdbg << code_entry;

    sen_s.disableStreamingBT(rvr::CommandResponse::resp_yes);
    sen_s.clearStreamingBT(rvr::CommandResponse::resp_yes);

    sen_s.streamQuaternion(rvr::CommandResponse::resp_yes);

    sen_s.startStreamingBT(50, rvr::CommandResponse::resp_yes);
    sen_s.wait_for( &rvr::SensorsStream::quaternion);

    sen_s.disableStreamingBT(rvr::CommandResponse::resp_yes);
    sen_s.clearStreamingBT(rvr::CommandResponse::resp_yes);

    auto [q_w, q_x, q_y, q_z] { sen_s.quaternion().get_or() };
    mys::tout << code_line << "quaternion: " << q_w << mys::sp << q_x << mys::sp << q_y << mys::sp << q_z;

    mys::tdbg << code_exit;
}
//---------------------------------------------------------------------------------------------------------------------
void streaming(rvr::SensorsStream& sen_s, rvr::SensorsDirect& sen_d) {
    mys::tdbg << code_entry;

    sen_s.disableStreaming();
    sen_s.clearAllStreaming();

    nordic_ambient_light(sen_s);
    nordic_color(sen_s, sen_d);
    nordic_time(sen_s);

    bt_imu_accel_gyro(sen_s);
    bt_time(sen_s);
    bt_speed_vel_loc(sen_s);
    bt_quaternion(sen_s);    //  ??

    sen_s.disableStreaming();
    sen_s.clearAllStreaming();

    mys::tdbg << code_exit;
}
