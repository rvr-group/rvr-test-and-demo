//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//		 File: direct.cpp
//
//     Author: rmerriam
//
//    Created: Jun 10, 2021
//
//======================================================================================================================
#include <thread>

#include <rvr++.h>
//---------------------------------------------------------------------------------------------------------------------
void color(rvr::SensorsDirect& sen_d);
//---------------------------------------------------------------------------------------------------------------------
void general(rvr::SensorsDirect& sen_d) {
    mys::tdbg << code_entry;

    sen_d.enableGyroMaxNotify();

    sen_d.resetLocatorXY();
    sen_d.setLocatorFlags(rvr::SensorsDirect::LocatorFlagsBitmask::auto_calibrate);

    sen_d.getAmbientLightSensorValue();
    sen_d.getEncoderCounts();

    sen_d.getLeftMotorTemp();
    sen_d.getRightMotorTemp();

    sen_d.getNordicTemp();

    sen_d.getThermalProtectionStatus();
    sen_d.enableThermalProtectionNotify();    // responds when status changes

    sen_d.wait_for( &rvr::SensorsDirect::thermalProtectionValues);

    mys::tout << code_line << "isGyroMaxNotifyEnabled: " << sen_d.isGyroMaxNotifyEnabled().get_or();
    mys::tout << code_line << "isThermalProtectionNotifyEnabled: " << sen_d.isThermalProtectionNotifyEnabled().get_or();

    mys::tout << code_line << "ambient light: " << std::setprecision(2) << sen_d.ambientLight().get_or();

    auto [left_count, right_count] { sen_d.encoderCounts().get_or() };
    mys::tout << code_line << "encoderCounts: " << left_count << mys::sp << right_count;

    auto [left_temp, left_status, right_temp, right_status] { sen_d.thermalProtectionValues().get_or() };
    mys::tout << code_line << "thermalProtectionValues: " << left_temp << mys::sp << (int)left_status    //
        << mys::sp << right_temp << mys::sp << (int)right_status;

    mys::tout << code_line << "Left Temp: " << sen_d.leftMotorTemp().get_or();
    mys::tout << code_line << "right Temp: " << sen_d.rightMotorTemp().get_or();
    mys::tout << code_line << "Nordic Temp: " << sen_d.nordicTemp().get_or();

    sen_d.disableGyroMaxNotify();
    sen_d.disableThermalProtectionNotify();

    sen_d.wait_for( &rvr::SensorsDirect::isThermalProtectionNotifyEnabled);

    mys::tout << code_line;
    mys::tout << code_line << "isGyroMaxNotifyEnabled: " << sen_d.isGyroMaxNotifyEnabled().get_or();
    mys::tout << code_line << "isThermalProtectionNotifyEnabled: " << sen_d.isThermalProtectionNotifyEnabled().get_or();

    mys::tdbg << code_exit;
}
//---------------------------------------------------------------------------------------------------------------------
void direct(rvr::SensorsDirect& sen_d) {
    mys::tdbg << code_entry;

    general(sen_d);
    color(sen_d);
    mys::tdbg << code_exit;
}
