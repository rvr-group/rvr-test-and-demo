//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//		 File: lock_test.cpp
//
//     Author: rmerriam
//
//    Created: Dec 17, 2021
//
//======================================================================================================================
#include <atomic>
#include <thread>

#include <rvr++.h>
//---------------------------------------------------------------------------------------------------------------------
void lock_test(rvr::Blackboard& bb, rvr::SensorsStream& sen_s) {

    sen_s.disableStreamingNordic(rvr::CommandResponse::resp_yes);
    sen_s.streamAmbient(rvr::CommandResponse::resp_yes);
    sen_s.startStreamingNordic(20, rvr::CommandResponse::resp_yes);

    sen_s.wait_for( &rvr::SensorsStream::ambient);

    for (auto cnt { 0 }; cnt < 20; ++cnt) {
        std::this_thread::sleep_for(20ms);

        auto value { sen_s.ambient().get_or() };

        mys::tout << code_line << "Ambient Light: "s << std::dec << value;
    }

    sen_s.disableStreamingNordic(rvr::CommandResponse::resp_yes);
    sen_s.clearAllStreaming(rvr::CommandResponse::resp_yes);

    mys::tdbg << code_exit;
}
