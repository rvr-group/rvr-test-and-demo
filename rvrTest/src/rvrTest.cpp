//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http:           //www.gnu.org/licenses/>.
//======================================================================================================================
//
//     Author: rmerriam
//
//     Created: May 29, 2021
//
//======================================================================================================================
#include <thread>
using namespace std::literals;

#include <Signal.h>

#include <Trace.h>
#include <rvr++.h>

//---------------------------------------------------------------------------------------------------------------------
void direct(rvr::SensorsDirect& sen_d);
void driving(rvr::Drive& drive, rvr::SensorsDirect& sen_d);
void lock_test(rvr::Blackboard& bb, rvr::SensorsStream& sen_s);
void notifications(rvr::SensorsDirect& sen_d);
void power(rvr::Power& pow);
void streaming(rvr::SensorsStream& sen_s, rvr::SensorsDirect& sen_d);
void specials(rvr::Blackboard& bb, rvr::ApiShell& api, rvr::SensorsDirect& sen_d, rvr::SensorsStream& sen_s);
void sysinfo(rvr::SystemInfo& sys, rvr::Connection& con, rvr::ApiShell& api);
//---------------------------------------------------------------------------------------------------------------------
void terminate(int signal) {
    mys::tout << code_line << signal << " received\n";
}
//---------------------------------------------------------------------------------------------------------------------
int main(int argc, char* argv[]) {
    mys::tout << code_line << argv[0] << " starting tests";

    mys::TraceOff tdbg_ctrl { mys::tdbg };
    //---------------------------------------------------------------------------------------------------------------------
    mys::tout << code_line;
    auto stream = rvr::Stream::make_stream(argc, argv);

    mys::tout << code_line << *stream;
    mys::tout.flush();

    if ( *stream) {
        mys::tdbg << code_line << "stream open: " << argv[1];
    }
    else {
        mys::tout << code_line << "could not open stream : " << argv[1] << mys::sp << ((argc == 3) ? *argv[2] : mys::sp) << mys::nl;
        return -10;
    }
    mys::tout.flush();

    rvr::ReadPacket responses { *stream };
    rvr::SendPacket requests { *stream };
    //---------------------------------------------------------------------------------------------------------------------
    rvr::Blackboard bb;
    rvr::ApiShell api(bb, requests);
    rvr::Connection conn(bb, requests);
    rvr::Drive drive(bb, requests);
    rvr::Power pow(bb, requests);
    rvr::SensorsDirect sen_d(bb, requests);
    rvr::SensorsStream sen_s(bb, requests);
    rvr::SystemInfo sys(bb, requests);

    rvr::Signal sig { pow, terminate, terminate };
    mys::tout.flush();
    //---------------------------------------------------------------------------------------------------------------------
    // wake up the RVR if aslweep
    pow.awaken();
    sen_d.disableEverything(rvr::CommandResponse::resp_yes);
    //---------------------------------------------------------------------------------------------------------------------
    //  Construct Response which will start response thread
    rvr::Response response { responses, bb };

    pow.wait_for( &rvr::Power::isAwake);
    mys::tdbg << code_line << "is awake:  " << pow.isAwake();
    std::this_thread::sleep_for(150ms);    // have to wait for RVR to become fully sane
    //=====================================================================================================================
    try {
#if 0
        // tests of underlying requirements for rvr++
//        lock_test(bb, sen_s);
//        specials(bb, api, sen_d, sen_s);
#endif
#if 0
        // tests of rvr++ capabilities
        // non-moving
        direct(sen_d);
        streaming(sen_s, sen_d);
        power(pow);
        sysinfo(sys, conn, api);
#endif
#if 1
        // moving
        driving(drive, sen_d);
#endif
        mys::tout << code_line << "tests done\n";
    }
    catch (std::exception& e) {
        mys::terr << code_line << e.what() << "=================================";
    }

    mys::tout << code_line << "waiting for sleep notify";
    mys::tout << code_line;

    pow.resetSleepNotify();
    pow.sleep(rvr::CommandResponse::resp_yes);

//    bb.dump();

    pow.wait_for( &rvr::Power::isDidSleepNotify);

    response.shutdown();

    mys::tout << code_line << argv[0] << " done\n";

    return 0;
}

