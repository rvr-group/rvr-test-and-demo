//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//		 File: color.cpp
//
//     Author: rmerriam
//
//    Created: Jun 10, 2021
//
//======================================================================================================================
#include <thread>
#include <rvr++.h>

using namespace std::literals;
/*
 * Test the varioius response formats that don't follow the standard patterm
 */
//---------------------------------------------------------------------------------------------------------------------
// test that requests that have same cmd but different proc work
void processor_specific_responses(rvr::ApiShell& api) {
    mys::tdbg << code_entry;

    static rvr::RvrMsg const dead { 0xDE, 0xAD, 0xFE, 0xED };

    api.echo(dead);
    api.wait_for( &rvr::ApiShell::echoNordic);

    mys::tout << code_line << "Echo BT: " << std::hex << api.echoBT().get_or();
    mys::tout << code_line << "Echo Nordic: " << std::hex << api.echoNordic().get_or();

    mys::tdbg << code_exit;
}
//---------------------------------------------------------------------------------------------------------------------
void color_notification(rvr::Blackboard& bb, rvr::SensorsDirect& sen_d) {

    sen_d.enableColorDetection(rvr::CommandResponse::resp_yes);
    sen_d.enableColorDetectionNotify(true, 100, 0);

    sen_d.wait_for( &rvr::SensorsDirect::colorDetectionValues);

    auto [d_r, d_g, d_b, conf, classification] { sen_d.colorDetectionValues().get_or() };
    mys::tout << code_line << "colorDetectionValues: " << (int)(d_r) << mys::sp << (int)(d_g) << mys::sp << (int)(d_b) << mys::sp << (int)(conf)
              << mys::sp << (int)(classification);

    sen_d.enableColorDetectionNotify(false, 50, 0);
    sen_d.disableColorDetection(rvr::CommandResponse::resp_yes);    // turns off bottom LEDs
    std::this_thread::sleep_for(500ms);
}
//---------------------------------------------------------------------------------------------------------------------
void stream_ambient_light(rvr::SensorsStream& sen_s) {
    mys::tdbg << code_entry;

    sen_s.disableStreaming();
    std::this_thread::sleep_for(50ms);

    sen_s.streamAmbient(rvr::CommandResponse::resp_yes);
    sen_s.streamImuAccelGyro(rvr::CommandResponse::resp_yes);

    sen_s.enableStreaming(50, rvr::CommandResponse::resp_yes);
    sen_s.wait_for( &rvr::SensorsStream::ambient);

    mys::tout << code_line << "ambient: "s << sen_s.ambient().get_or();

    sen_s.wait_for( &rvr::SensorsStream::accelerometer);

    auto [i_p, i_r, i_y] { sen_s.imu().get_or() };
    mys::tout << code_line << "imu: " << i_p << mys::sp << i_r << mys::sp << i_y;

    auto [a_x, a_y, a_z] { sen_s.accelerometer().get_or() };
    mys::tout << code_line << "accelerometer: " << a_x << mys::sp << a_y << mys::sp << a_z;

    auto [g_x, g_y, g_z] { sen_s.gyroscope().get_or() };
    mys::tout << code_line << "gyroscope: " << g_x << mys::sp << g_y << mys::sp << g_z;

    sen_s.disableStreaming();
    std::this_thread::sleep_for(500ms);

    mys::tdbg << code_exit;
}
//---------------------------------------------------------------------------------------------------------------------
void simple_response(rvr::SensorsDirect& sen_d) {
    //    processor_specific_responses(api);
    //    color_notification(bb, sen_d);
    sen_d.getRgbcSensorValue();
    sen_d.wait_for( &rvr::SensorsDirect::currentRGBValues);

    auto [c_r, c_g, c_b, c_c] { sen_d.currentRGBValues().get_or() };
    mys::tout << code_line << "currentRGBValues: " << std::hex << c_r << mys::sp << c_g << mys::sp << c_b << mys::sp << c_c;
}
//---------------------------------------------------------------------------------------------------------------------
void specials(rvr::Blackboard& bb, rvr::ApiShell& api, rvr::SensorsDirect& sen_d, rvr::SensorsStream& sen_s) {
    mys::tdbg << code_entry;

    simple_response(sen_d);
    processor_specific_responses(api);
    color_notification(bb, sen_d);
    stream_ambient_light(sen_s);

    mys::tdbg << code_exit;
}
